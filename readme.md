# future-result

A Rust crate that provides an ideomatic way of mapping a `Result` to a `Future` of `Result`.

# Example usage

```rust
use future_result::FutureResult;

async fn add_1(x: u32) -> u32 {
    x + 1
}

fn main() {
    let fut = async {
        let ok: Result<u32, ()> = Ok(41);
        let ok = ok.then_map(add_1).await;

        assert_eq!(Ok(42), ok);

        // ...

        let err: Result<(), u32> = Err(9);
        let err = err.then_map_err(add_1).await;

        assert_eq!(Err(10), err);
    };

    futures::executor::block_on(fut);
}
```
